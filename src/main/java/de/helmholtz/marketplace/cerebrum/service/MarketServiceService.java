package de.helmholtz.marketplace.cerebrum.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatch;
import lombok.SneakyThrows;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import de.helmholtz.marketplace.cerebrum.entity.MarketService;
import de.helmholtz.marketplace.cerebrum.repository.MarketServiceRepository;
import de.helmholtz.marketplace.cerebrum.service.common.CerebrumServiceBase;
import de.helmholtz.marketplace.cerebrum.service.common.ForeignKeyExecutorService;

@Service
public class MarketServiceService extends CerebrumServiceBase<MarketService, MarketServiceRepository, ForeignKeyExecutorService>
{
    private final MarketServiceRepository marketServiceRepository;
    private final ForeignKeyExecutorService foreignKeyExecutorService;

    public MarketServiceService(MarketServiceRepository marketServiceRepository,
                                ForeignKeyExecutorService foreignKeyExecutorService)
    {
        super(MarketService.class, MarketServiceRepository.class, ForeignKeyExecutorService.class);
        this.marketServiceRepository = marketServiceRepository;
        this.foreignKeyExecutorService = foreignKeyExecutorService;
    }

    public Page<MarketService> getServices(PageRequest page)
    {
        return getAllEntities(page, marketServiceRepository);
    }

    public MarketService getService(String uuid)
    {
        return getEntity(uuid, marketServiceRepository);
    }

    public MarketService getServiceByAttributes(String attr, String value)
    {
        return getEntity(attr, value, marketServiceRepository);
    }

    public ResponseEntity<MarketService> createService(
            MarketService entity, UriComponentsBuilder uriComponentsBuilder)
    {
        return createEntity(entity, marketServiceRepository, foreignKeyExecutorService, uriComponentsBuilder);
    }

    public ResponseEntity<MarketService> updateService(
            String uuid, MarketService entity, UriComponentsBuilder uriComponentsBuilder)
    {
        return updateEntity(uuid, entity, marketServiceRepository, foreignKeyExecutorService, uriComponentsBuilder);
    }

    public ResponseEntity<MarketService> partiallyUpdateService(String uuid, JsonPatch patch)
    {
        return partiallyUpdateEntity(uuid, marketServiceRepository, foreignKeyExecutorService, patch);
    }

    public ResponseEntity<MarketService> deleteService(String uuid)
    {
        return deleteEntity(uuid, marketServiceRepository, foreignKeyExecutorService);
    }

    //service-provider
    public ResponseEntity<MarketService> addProvider(String serviceUuid, String providerUuid)
    {
        return provider(serviceUuid, providerUuid, true);
    }

    public ResponseEntity<MarketService> deleteProvider(String serviceUuid, String providerUuid)
    {
        return provider(serviceUuid, providerUuid, false);
    }

    @SneakyThrows
    private ResponseEntity<MarketService> provider(String serviceUuid, String providerUuid, boolean toAdd)
    {
        MarketService retrievedService = getService(serviceUuid);

        ObjectMapper objectMapper = new ObjectMapper();
        MarketService submittedService = objectMapper
                .readValue(objectMapper.writeValueAsString(retrievedService), MarketService.class);
        if (toAdd) {
            if (retrievedService.getServiceProviders().contains(providerUuid)) {
                return ResponseEntity.noContent().build();
            }
            submittedService.addProvider(providerUuid);
        } else {
            if (!retrievedService.getServiceProviders().contains(providerUuid)) {
                return ResponseEntity.noContent().build();
            }
            submittedService.removeProvider(providerUuid);
        }


        foreignKeyExecutorService.executeBeforeUpdate(retrievedService, submittedService);
        MarketService updatedService = marketServiceRepository.save(submittedService);
        return ResponseEntity.ok().body(updatedService);
    }

    //Service Management Team
    public ResponseEntity<MarketService> addTeamMember(String serviceUuid, String personUuid)
    {
        return teamMember(serviceUuid, personUuid, true);
    }

    public ResponseEntity<MarketService> deleteTeamMember(String serviceUuid, String personUuid)
    {
        return teamMember(serviceUuid, personUuid, false);
    }

    @SneakyThrows
    private ResponseEntity<MarketService> teamMember(String serviceUuid, String personUuid, boolean toAdd)
    {
        MarketService retrievedService = getService(serviceUuid);

        ObjectMapper objectMapper = new ObjectMapper();
        MarketService submittedService = objectMapper
                .readValue(objectMapper.writeValueAsString(retrievedService), MarketService.class);

        if (toAdd) {
            if (retrievedService.getManagementTeam().contains(personUuid)) {
                return ResponseEntity.noContent().build();
            }
            submittedService.addProvider(personUuid);
        } else {
            if (!retrievedService.getManagementTeam().contains(personUuid)) {
                return ResponseEntity.noContent().build();
            }
            submittedService.removeProvider(personUuid);
        }

        foreignKeyExecutorService.executeBeforeUpdate(retrievedService, submittedService);
        MarketService updatedService = marketServiceRepository.save(submittedService);
        return ResponseEntity.ok().body(updatedService);
    }
}
