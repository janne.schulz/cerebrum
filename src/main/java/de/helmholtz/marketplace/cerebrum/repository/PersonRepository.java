package de.helmholtz.marketplace.cerebrum.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

import de.helmholtz.marketplace.cerebrum.entity.Person;
import de.helmholtz.marketplace.cerebrum.repository.fragment.CerebrumRepository;

public interface PersonRepository extends MongoRepository<Person, String>, CerebrumRepository<Person>
{
    Optional<Person> findByUuid(@Param("uuid") String uuid);
    Optional<Person> deleteByUuid(@Param("uuid") String uuid);
    Person findByFirstNameAndLastName(@Param("firstName") String firstName, @Param("lastName") String lastName);
}
