package de.helmholtz.marketplace.cerebrum.repository.fragment;

import java.util.Set;

public interface CerebrumRepository<T>
{
    void addForeignKey(String uuid, String key);
    void removeForeignKey(String uuid, String key);
    T updateForeignKeys(String uuid, Set<String> keys);
}
