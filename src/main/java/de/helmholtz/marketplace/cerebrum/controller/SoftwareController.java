package de.helmholtz.marketplace.cerebrum.controller;

import com.github.fge.jsonpatch.JsonPatch;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;

import de.helmholtz.marketplace.cerebrum.entity.Software;
import de.helmholtz.marketplace.cerebrum.errorhandling.CerebrumApiError;
import de.helmholtz.marketplace.cerebrum.service.SoftwareService;
import de.helmholtz.marketplace.cerebrum.utils.CerebrumControllerUtilities;

@RestController
@Validated
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE,
        path = "${spring.data.rest.base-path}/software")
@Tag(name = "software", description = "The Software API")
public class SoftwareController
{
    private final SoftwareService softwareService;

    public SoftwareController(SoftwareService softwareService)
    {
        this.softwareService = softwareService;
    }

    /* get list of software */
    @Operation(summary = "get array list of all software")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "successful operation",
                    content = @Content(array = @ArraySchema(
                            schema = @Schema(implementation = Software.class)))),
            @ApiResponse(responseCode = "400", description = "invalid request",
                    content = @Content(array = @ArraySchema(
                            schema = @Schema(implementation = CerebrumApiError.class))))
    })
    @GetMapping(path = "")
    public Iterable<Software> getSoftware(
            @Parameter(description = "specify the name of the software to search for")
            @RequestParam(value = "name") String name,
            @Parameter(description = "specify the page number")
            @RequestParam(value = "page", defaultValue = "0") @Min(0) Integer page,
            @Parameter(description = "limit the number of records returned in one page")
            @RequestParam(value = "size", defaultValue = "20") @Min(1) Integer size,
            @Parameter(description = "sort the fetched data in either ascending (asc) " +
                    "or descending (desc) according to one or more of the software " +
                    "properties. Eg. to sort the list in ascending order base on the " +
                    "name property; the value will be set to name.asc")
            @RequestParam(value = "sort", defaultValue = "name.asc") List<String> sorts)
    {
        return (name == null || name.isEmpty()) ?
                softwareService.getSoftware(PageRequest.of(page, size,
                        Sort.by(CerebrumControllerUtilities.getOrders(sorts)))) :
                softwareService.getSoftware(name, PageRequest.of(page, size,
                        Sort.by(CerebrumControllerUtilities.getOrders(sorts))));
    }

    /* get Software */
    @Operation(summary = "find software by ID",
            description = "Returns a detailed software information " +
                    "corresponding to the ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "successful operation",
                    content = @Content(schema = @Schema(implementation = Software.class))),
            @ApiResponse(responseCode = "400", description = "invalid software ID supplied",
                    content = @Content(schema = @Schema(implementation = CerebrumApiError.class))),
            @ApiResponse(responseCode = "404", description = "software not found",
                    content = @Content(schema = @Schema(implementation = CerebrumApiError.class)))
    })
    @GetMapping(path = "/{uuid}")
    public Software getSoftware(
            @Parameter(description = "ID of the software that needs to be fetched")
            @PathVariable(name = "uuid") String uuid)
    {
        return softwareService.getSoftware(uuid);
    }

    /* create Software */
    @PreAuthorize("isAuthenticated()")
    @Operation(summary = "add a new software",
            security = @SecurityRequirement(name = "hdf-aai"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "software created",
                    content = @Content(schema = @Schema(implementation = Software.class))),
            @ApiResponse(responseCode = "400", description = "invalid ID supplied",
                    content = @Content(schema = @Schema(implementation = CerebrumApiError.class))),
            @ApiResponse(responseCode = "401", description = "unauthorised", content = @Content())
    })
    @PostMapping(path = "", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Software> createSoftware(
            @Parameter(description = "software object that needs to be added to the marketplace",
                    required = true, schema = @Schema(implementation = Software.class))
            @Valid @RequestBody Software software, UriComponentsBuilder uriComponentsBuilder)
    {
        return softwareService.createSoftware(software, uriComponentsBuilder);
    }

    /* update Software */
    @PreAuthorize("isAuthenticated()")
    @Operation(summary = "update an existing software",
            description = "Update part (or all) of an software information",
            security = @SecurityRequirement(name = "hdf-aai"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation",
                    content = @Content(schema = @Schema(implementation = Software.class))),
            @ApiResponse(responseCode = "201", description = "software created",
                    content = @Content(schema = @Schema(implementation = Software.class))),
            @ApiResponse(responseCode = "400", description = "invalid ID supplied",
                    content = @Content(schema = @Schema(implementation = CerebrumApiError.class))),
            @ApiResponse(responseCode = "401", description = "unauthorised", content = @Content())
    })
    @PutMapping(path = "/{uuid}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Software> updateSoftware(
            @Parameter(description="Software to update or replace. This cannot be null or empty.",
                    schema=@Schema(implementation = Software.class),
                    required=true) @Valid @RequestBody Software newSoftware,
            @Parameter(description = "Unique identifier of the software that needs to be updated")
            @PathVariable(name = "uuid") String uuid, UriComponentsBuilder uriComponentsBuilder)
    {
        return softwareService
                .updateSoftware(uuid, newSoftware, uriComponentsBuilder);
    }

    /* JSON PATCH Software */
    @PreAuthorize("isAuthenticated()")
    @Operation(summary = "partially update an existing software",
            security = @SecurityRequirement(name = "hdf-aai"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation",
                    content = @Content(schema = @Schema(implementation = Software.class))),
            @ApiResponse(responseCode = "400", description = "invalid id or json patch body",
                    content = @Content(schema = @Schema(implementation = CerebrumApiError.class))),
            @ApiResponse(responseCode = "401", description = "unauthorised", content = @Content()),
            @ApiResponse(responseCode = "404", description = "software not found",
                    content = @Content(schema = @Schema(implementation = CerebrumApiError.class))),
            @ApiResponse(responseCode = "500", description = "internal server error",
                    content = @Content(schema = @Schema(implementation = CerebrumApiError.class)))
    })
    @PatchMapping(path = "/{uuid}", consumes = "application/json-patch+json")
    public ResponseEntity<Software> partialUpdateSoftware(
            @Parameter(description = "JSON Patch document structured as a JSON " +
                    "array of objects where each object contains one of the six " +
                    "JSON Patch operations: add, remove, replace, move, copy, and test",
                    schema = @Schema(implementation = JsonPatch.class),
                    required = true) @Valid @RequestBody JsonPatch patch,
            @Parameter(description = "ID of the software that needs to be partially updated")
            @PathVariable(name = "uuid") String uuid)
    {
        return softwareService.partiallyUpdateSoftware(uuid, patch);
    }

    /* delete Software */
    @PreAuthorize("isAuthenticated()")
    @Operation(summary = "deletes an software",
            description = "Removes the record of the specified " +
                    "software id from the database. The software " +
                    "unique identification number cannot be null or empty",
            security = @SecurityRequirement(name = "hdf-aai"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "successful operation", content = @Content()),
            @ApiResponse(responseCode = "401", description = "unauthorised", content = @Content())
    })
    @DeleteMapping(path = "/{uuid}")
    public ResponseEntity<Software> deleteSoftware(
            @Parameter(description="software id to delete", required=true)
            @PathVariable(name = "uuid") String uuid)
    {
        return softwareService.deleteSoftware(uuid);
    }
}
