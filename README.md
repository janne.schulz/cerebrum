HIFIS Cloud Portal Cerebrum
===========================
![](https://github.com/helmholtz-marketplace/helmholtz-cerebrum/workflows/CI/badge.svg)

Cerebrum is the resources API for Helmholtz Cloud Portal.

### Automatic deployments of Merge Requests

Each Merge Request will be automatically deployed to a Kubernetes test cluster so that you can directly see your changes in a live deployment. It will be set up together with the Webapp frontend and its own database. For the Webapp the code from its current master branch will be taken. The deployment is running in a pipeline at DESY. You can find a link to that pipeline in the output of the last job in the CI/CD pipeline (`trigger_deploy_mr`). There you can also find a url where you access the Cloud Portal when it has been deployed. When you push new commits to the MR the application will be redeployed.

### Building and Running

In order to build and run Helmholtz Marketplace Cerebrum you will need
* OpenJDK 11
* Apache Maven 3.6

To start you will need to clone the project and then execute
```
mvn clean install
```

in order to run the project, build it and execute the Jar file from the command line:
```
java -jar target/helmholtz-cerebrum-<version number>-SNAPSHOT.jar
```

Since this is an API-only application, you will need to access the API endpoints in order to see something from the functionality. A startingpoint is

[http://localhost:8090/api/v0](http://localhost:8090/api/v0)

which should show you the existing endpoints.
